﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SumPairsQuiz.Logic
{
    public static class Extensions
    {
        /// <summary>
        /// Gets all pairs of elements, which sum equals to the "sum" parameter
        /// Only pairs with unique elements count, where uniqueness refers to combination of value and index
        /// </summary>
        /// <param name="numbers">elements</param>
        /// <param name="sum">value of the sum for pairs to match</param>
        /// <returns>Enumerable of tuples with pairs</returns>
        public static IEnumerable<Pair> GetAllPairs(this IEnumerable<int> numbers, int sum)
        {
            var complementariesIndiciesMap = new Dictionary<int, List<int>>();
            var tuplesIndiciesHashSet = new HashSet<Pair>();

            var index = 0;
            foreach (var number in numbers)
            {
                var complementaryNumber = sum - number;
                if (complementariesIndiciesMap.ContainsKey(complementaryNumber))
                    complementariesIndiciesMap[complementaryNumber].Add(index);
                else
                    complementariesIndiciesMap[complementaryNumber] = new List<int>() { index };
                index++;
            }

            index = 0;
            foreach(var number in numbers)
            {
                if (complementariesIndiciesMap.ContainsKey(number))
                {
                    foreach(var item in complementariesIndiciesMap[number].Where(x=>index !=x))
                    {
                        var pair = new Pair(number, sum - number);
                        var indiciesPair = new Pair(index, item);

                        Console.WriteLine(pair);
                        if (!tuplesIndiciesHashSet.Contains(indiciesPair))
                        {
                            tuplesIndiciesHashSet.Add(indiciesPair);
                            yield return pair;
                        }
                    }
                }
                index++;
            }
        }
    }
    
    public struct Pair
    {
        public int Item1 { get; private set; }
        public int Item2 { get; private set; }

        public Pair(int value1, int value2)
        {
            Item1 = value1;
            Item2 = value2;
        }

        public override int GetHashCode()
        {
            //Pairs like [a,b] and [b,a] are treated as the same
            return Item1.GetHashCode() ^ Item2.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
                return false;
            if (ReferenceEquals(this, obj))
                return true;
            if (obj.GetType() != GetType())
                return false;

            var pair = (Pair)obj;

            //Pairs like [a,b] and [b,a] are treated as the same
            return (Item1.Equals( pair.Item1) && Item2.Equals(pair.Item2)) || (Item1.Equals(pair.Item2) && Item2.Equals(pair.Item1));
        }

        public override string ToString()
        {
            return string.Format("[{0},{1}]", Item1, Item2);
        }
    }

}
