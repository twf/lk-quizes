﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SumPairsQuiz.Logic;
using System.Linq;

namespace SumPairsQuiz.Tests
{
    [TestClass]
    public class SumPairsTests
    {
        [TestMethod]
        public void PairsAreFound_OnCollectionWithNonUniqueElements()
        {
            CollectionAssert.AreEquivalent(new Pair[] {
                new Pair(1,3),
                new Pair(4,0),
                new Pair(4,0),
                new Pair(5,-1),
            }, new int[] { 1, 2, 3, 0, 5, 6, 7, -1, 4, 0 }.GetAllPairs(4).Select(x=>new Pair(x.Item1, x.Item2)).ToList());


        }

        [TestMethod]
        public void PairsAreFound_OnAscendedCollectionWithNonUniqueElements()
        {
            CollectionAssert.AreEquivalent(new Pair[] {
                new Pair(1,3),
                new Pair(4,0),
                new Pair(4,0),
                new Pair(5,-1),
            }, new int[] { 1, 2, 3, 0, 5, 6, 7, -1, 4, 0 }.OrderBy(x => x).GetAllPairs(4).ToList());

        }

        [TestMethod]
        public void PairsAreFound_OnDescendedCollectionWithNonUniqueElements()
        {
            CollectionAssert.AreEquivalent(new Pair[] {
                new Pair(1,3),
                new Pair(4,0),
                new Pair(4,0),
                new Pair(5,-1),
            }, new int[] { 1, 2, 3, 0, 5, 6, 7, -1, 4, 0 }.OrderByDescending(x => x).GetAllPairs(4).ToList());

        }

        [TestMethod]
        public void PairsCombinationsAreFound_OnRepetativeElemets()
        {
            /*
             * There are 4 combinations of pairs that can sum up to 4
             */
            CollectionAssert.AreEquivalent(Enumerable.Range(0, 4).Select(x=>new Pair(1,3)).ToArray(), 
                new int[] { 1, 3, 1, 3 }.GetAllPairs(4).ToList());
        }


        [TestMethod]
        public void PairsCombinationsAreFound_OnFourRepetativeElemets()
        {
            /*
             * There are 16 combinations of pairs that can sum up to 4
             */
            CollectionAssert.AreEquivalent(Enumerable.Range(0, 16).Select(x => new Pair(1, 3)).ToArray(),
                new int[] { 1, 3, 1, 3, 1, 3, 1, 3 }.GetAllPairs(4).ToList());
        }

        [TestMethod]
        public void PairsAreFound_OnUniqueElements()
        {
            CollectionAssert.AreEquivalent(new Pair[] {
                new Pair(1,3),
                new Pair(4,0),
                new Pair(5,-1),
            }, new int[] { 1, 2, 3, 0, 5, 6, 7, -1, 4 }.OrderBy(x => x).GetAllPairs(4).ToList());

        }

        [TestMethod]
        public void SinglePairIsFound_OnTwoRepetativeSameHalvedElements()
        {
            CollectionAssert.AreEquivalent(Enumerable.Range(0, 1).Select(x => new Pair(2, 2)).ToArray(),
                new int[] { 2, 2 }.GetAllPairs(4).ToList());

        }

        [TestMethod]
        public void NoPairsAreFound_OnEmptyCollection()
        {
            Assert.IsFalse(new int[] { }.GetAllPairs(4).Any());
        }

        [TestMethod]
        public void NoPairsAreFound_OnSingleElementCollection()
        {
            Assert.IsFalse(new int[] { 4 }.GetAllPairs(4).Any());
        }

        [TestMethod]
        public void NoPairsAreFound_OnCollectionWithoutMatchingElements()
        {
            Assert.IsFalse(new int[] { 1, 2, 4, 6, 7, 8, 9 }.GetAllPairs(4).Any());
        }



    }
}
