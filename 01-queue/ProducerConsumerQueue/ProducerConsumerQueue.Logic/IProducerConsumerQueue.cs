﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace ProducerConsumerQueue.Logic
{
    public interface IProducerConsumerQueue<T>
    {
        T Pop();
        void Push(T value);
    }

    public class MutexProducerConsumerQueue<T> : IProducerConsumerQueue<T>
    {
        private readonly Queue<T> _queue;
        private readonly object _syncRoot = new object();

        public MutexProducerConsumerQueue()
        {
            _queue = new Queue<T>();
        }

        public T Pop()
        {
            lock (_syncRoot)
            {
                while(_queue.Count == 0)
                {
                    Monitor.Wait(_syncRoot);
                }
                return _queue.Dequeue();
            }
        }

        public void Push(T value)
        {
            lock (_syncRoot)
            {
                _queue.Enqueue(value);
                Monitor.Pulse(_syncRoot);
            }
        }
    }
}
