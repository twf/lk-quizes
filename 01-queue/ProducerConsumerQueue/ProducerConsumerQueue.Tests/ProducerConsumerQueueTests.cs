﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading;
using ProducerConsumerQueue.Logic;
using System.Linq;
using System.Collections.Concurrent;

namespace ProducerConsumerQueue.Tests
{
    [TestClass]
    public class ProducerConsumerQueueTests
    {

        [TestMethod]
        public void ItemsCanBePushedAndPopedSynchronously()
        {
            var values = Enumerable.Range(1, 20);
            var queue = new MutexProducerConsumerQueue<int>();

            foreach (var value in values)
            {
                queue.Push(value);
                Assert.AreEqual(queue.Pop(), value);
            }
        }

        [TestMethod]
        public void ItemCanBePoped_OnlyAfterItIsPushed()
        {
            var sampleValue = 42;
            var valueToPop = 0;
            var popDateTime = DateTime.MinValue;
            var pushDateTime = DateTime.MinValue;
            var consumerThreadStartedTime = DateTime.MinValue;
            var producerSleepTime = 1000;

            var queue = new MutexProducerConsumerQueue<int>();
            var consumerThread = new Thread(()=> {
                consumerThreadStartedTime = DateTime.Now;
                valueToPop = queue.Pop();
                popDateTime = DateTime.Now;
            });

            var producerThread = new Thread(() => {
                pushDateTime = DateTime.Now;
                Thread.Sleep(producerSleepTime);
                queue.Push(sampleValue);
            });


            consumerThread.Start();
            producerThread.Start();

            consumerThread.Join();
            producerThread.Join();

            Assert.AreEqual(sampleValue, valueToPop);
            //pop operation must have been waiting for at least the push operation sleeping times
            Assert.IsTrue(popDateTime.Subtract(consumerThreadStartedTime).TotalMilliseconds >= producerSleepTime);
        }

        [TestMethod]
        public void StochasticLoadTest()
        {
            var queue = new MutexProducerConsumerQueue<int>();
            var rnd = new Random();
            var maximumDelay = 1000;
            int producersAndConsumersCount = 20;

            var valuesToProduce = Enumerable.Range(1, producersAndConsumersCount).ToArray();
            var valuesConsumed = new ConcurrentBag<int>();

            int valueConsumedCount = 0;

            var consumerThread = Enumerable.Range(0, producersAndConsumersCount).Select(i => new Thread(() => {

                //some consumers are going to consume with delay, some are going to do it immediately 
                var sleepingTime = i % 3 == 0 ? 0 : rnd.Next(maximumDelay);
                Console.WriteLine("Thread[{1}]:[{0}] Sleeping for {2} msec", DateTime.Now.ToString("s"), Thread.CurrentThread.ManagedThreadId, sleepingTime);
                Thread.Sleep(sleepingTime);
                Console.WriteLine("Thread[{1}]:[{0}] Taking a value from the queue", DateTime.Now.ToString("s"), Thread.CurrentThread.ManagedThreadId);
                var value = queue.Pop();
                Console.WriteLine("Thread[{1}]:[{0}] Value taken!", DateTime.Now.ToString("s"), Thread.CurrentThread.ManagedThreadId);
                Interlocked.Increment(ref valueConsumedCount);
                valuesConsumed.Add(value);
                
            })).ToList();

            var producerThread = Enumerable.Range(0, producersAndConsumersCount).Select(i => new Thread(() => {
                //all producers are going to push with some random delay
                var sleepingTime = rnd.Next(maximumDelay);
                Console.WriteLine("Thread[{1}]:[{0}] Sleeping for {2} msec", DateTime.Now.ToString("s"), Thread.CurrentThread.ManagedThreadId, sleepingTime);
                Thread.Sleep(sleepingTime);
                Console.WriteLine("Thread[{1}]:[{0}] Producing the value", DateTime.Now.ToString("s"), Thread.CurrentThread.ManagedThreadId);
                queue.Push(valuesToProduce[i]);
                Console.WriteLine("Thread[{1}]:[{0}] Value produced!", DateTime.Now.ToString("s"), Thread.CurrentThread.ManagedThreadId);
            })).ToList();


            consumerThread.ForEach(x => x.Start());
            producerThread.ForEach(x => x.Start());

            consumerThread.ForEach(x => x.Join());
            producerThread.ForEach(x => x.Join());

            Assert.AreEqual(producersAndConsumersCount, valueConsumedCount);
            CollectionAssert.AreEquivalent(valuesToProduce, valuesConsumed);
        }
    }
}
